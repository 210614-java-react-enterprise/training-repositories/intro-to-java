package dev.rehm.datastructures;

public class ListApp {

    public static void main(String[] args) {
        MyList list = new MyLinkedList();
        list.add(-10);
        System.out.println(list.size());
        list.add(10);
        System.out.println(list.size());
        list.add(4);
        System.out.println(list.size());
        // [4,10]

        System.out.println(list.remove(4));
        System.out.println(list.remove(1));
        System.out.println(list.remove(0));
    }
}
