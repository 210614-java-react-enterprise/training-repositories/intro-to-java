package dev.rehm.datastructures;

public interface MyList {

    /*
        adds a value to the beginning of the list, this operation is only successful if our value is positive
        (returns true if value was added successfully, and false otherwise)
     */
    boolean add(int value);

    /*
        removes the value associated with the provided index
     */
    int remove(int index);

    /*
        returns the number of values currently stored in the list
     */
    int size();



}
