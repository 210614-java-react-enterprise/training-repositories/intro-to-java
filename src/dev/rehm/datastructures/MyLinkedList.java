package dev.rehm.datastructures;

public class MyLinkedList implements MyList {

    private int size;
    private Node head;
    private Node tail;

    @Override
    public boolean add(int value) {
        if(value<0){
            return false;
        }
        Node newNode = new Node(value);
        newNode.next = head;
        head = newNode;
        if(size==0){
            tail = newNode;
        }
        size++;
        return true;
    }

    @Override
    public int remove(int index) {
        if(index>=size || index<0){
            return -1;
        }

        int removedValue;

        // if you remove the head
        if(index==0){
            removedValue = head.data;
            head = head.next;
            // return removed value
            size--;
            return removedValue;
        }

        // if we remove a node somewhere in the middle (not head or tail)
        Node parentNode = head;
        for(int i=0; i<index-1; i++){
            parentNode = parentNode.next;
        }
        removedValue = parentNode.next.data;
        // if we removed the tail node, we would need to reset the tail to the previous node
        if(parentNode.next.next == null){
            tail = parentNode;
        }
        parentNode.next = parentNode.next.next;
        size--;
        return removedValue;
    }

    @Override
    public int size() {
        return size;
    }


    class Node{
        int data;
        Node next;

        Node(int data){
            super();
            this.data = data;
        }
    }
}
