package dev.rehm.models;

public enum Size {

    XS, S, M, L, XL

}
