package dev.rehm.models;

import java.util.Arrays;
import java.util.Objects;

public class Shirt extends Garment {

    private SleeveLength sleeves;
    private boolean hasCollar;

    public Shirt(){
        super();
    }

    @Override
    public void fold() {
        System.out.println("folding shirt");
    }

    public Shirt(String color, Size size, double price, SleeveLength sleeves, boolean hasCollar){
        super(color, size, price);
        this.sleeves = sleeves;
        this.hasCollar = hasCollar;
    }

    public SleeveLength getSleeves() {
        return sleeves;
    }

    public void setSleeves(SleeveLength sleeves) {
        this.sleeves = sleeves;
    }

    public boolean isHasCollar() {
        return hasCollar;
    }

    public void setHasCollar(boolean hasCollar) {
        this.hasCollar = hasCollar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Shirt shirt = (Shirt) o;
        return hasCollar == shirt.hasCollar && sleeves == shirt.sleeves;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), sleeves, hasCollar);
    }

    @Override
    public String toString() {
        return "Shirt{" +
                "color='" + getColor() + '\'' +
                ", hasCollar="+ hasCollar +
                ", sleeves=" +sleeves+
                ", size=" + getSize() +
                ", price=" + getPrice() +
                ", materials=" + Arrays.toString(getMaterials()) +
                '}';
    }

}
