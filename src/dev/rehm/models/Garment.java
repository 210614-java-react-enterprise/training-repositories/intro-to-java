package dev.rehm.models;

import dev.rehm.exceptions.NegativePriceException;

import java.util.Arrays;
import java.util.Objects;

public abstract class Garment implements Foldable {

    private String color; // default of null because it's in the instance scope
//    String size = "really big";
    private Size size = Size.L;
    private double price;
    private Material[] materials; // new ShirtMaterial[3];

    public Garment(String color, Size size, double price){
        super();
        this.color = color;
        this.size = size;
        setPrice(price);
    }

    // once we wrote a constructor, we were no longer given a no argument constructor and needed to define one
    // ourselves
    public Garment(){
        super();
    }

    public String getColor(){
        return color;
    }

    public void setColor(String color){
        this.color = color;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if(price>0){
            this.price = price;
        } else {
            throw new NegativePriceException("Cannot set price to value: "+price);
        }
    }

    public Material[] getMaterials() {
        return materials;
    }

    /*
    public void setMaterials(ShirtMaterial material){
        materials[0] = material;
    }

    public void setMaterials(ShirtMaterial material0, ShirtMaterial material1){
        materials[0] = material0;
        materials[1] = material1;
    }

    public void setMaterials(ShirtMaterial material0, ShirtMaterial material1, ShirtMaterial material2){
        materials[0] = material0;
        materials[1] = material1;
        materials[2] = material2;
    }
     */

    public void setMaterials(Material... materials){ // materials of type ShirtMaterial[]
        if(materials.length<4){ // if there are 3 or less materials input, we can just assign the array we're given
            this.materials = materials;
        } else { // if we get more than 3 materials, we only take the first three
            this.materials = new Material[3];
            for(int i = 0; i<this.materials.length;i++){
                this.materials[i] = materials[i];
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Garment garment = (Garment) o;
        return Double.compare(garment.price, price) == 0 && Objects.equals(color, garment.color) && size == garment.size && Arrays.equals(materials, garment.materials);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(color, size, price);
        result = 31 * result + Arrays.hashCode(materials);
        return result;
    }

    @Override
    public String toString() {
        return "Shirt{" +
                "color='" + color + '\'' +
                ", size=" + size +
                ", price=" + price +
                ", materials=" + Arrays.toString(materials) +
                '}';
    }
}
