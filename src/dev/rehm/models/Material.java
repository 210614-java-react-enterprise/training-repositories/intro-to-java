package dev.rehm.models;

public enum Material {

    COTTON, WOOL, POLYESTER, SILK, NYLON
}
