package dev.rehm.models;

public enum SleeveLength {

    SLEEVELESS, SHORT_SLEEVE, LONG_SLEEVE

}
