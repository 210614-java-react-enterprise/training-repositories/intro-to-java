package dev.rehm.models;

public interface Foldable {

    public void fold();
}
