package dev.rehm.exceptions;

public class NegativePriceException extends RuntimeException {

    public NegativePriceException(){
        super();
    }

    public NegativePriceException(String message){
        super(message);
    }
}
