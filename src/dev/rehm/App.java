package dev.rehm;

import dev.rehm.exceptions.NegativePriceException;
import dev.rehm.models.*;

import java.io.IOException;

public class App {

    public static void main(String[] args) {
        /*
//        dev.rehm.models.Shirt shirt = new dev.rehm.models.Shirt();
        Garment shirt = new Garment();
        shirt.setMaterials(Material.WOOL);
        Garment shirt2 = new Garment("blue", Size.M, 20.00);
        shirt2.setMaterials(Material.COTTON, Material.POLYESTER);

        System.out.println(shirt);
        System.out.println(shirt2.toString());

        shirt2.setMaterials(Material.COTTON, Material.POLYESTER, Material.NYLON, Material.WOOL);
        System.out.println(shirt2.toString());

//        shirt.color = "Green";

        Object shirtObject = new Garment();
//        System.out.println(shirtObject.toString());

         */

        /*
        Shirt s = new Shirt("black", Size.S, 15.99, SleeveLength.SHORT_SLEEVE, true);
        System.out.println(s);
        s.setMaterials(Material.COTTON);
        System.out.println(s);

        Garment s2 = new Shirt("green", Size.XL, 14.99, SleeveLength.LONG_SLEEVE, false);
        s2.getColor();
        Foldable s3 = new Shirt("blue", Size.L, 14.99, SleeveLength.LONG_SLEEVE, false);
        s3.fold();
         */

        Shirt s1 = new Shirt("black", Size.S, 15.99, SleeveLength.SHORT_SLEEVE, true);
        Shirt s2 = new Shirt("black", Size.S, 15.99, SleeveLength.SHORT_SLEEVE, true);
        System.out.println("s1 == s2 : "+ (s1 == s2));
        System.out.println("s1.equals(s2) : "+s1.equals(s2));


        s2.setPrice(-20);

//        throw new NullPointerException(); // this is a runtime exception (unchecked)
//        throw new IOException(); // this is a compile time exception (checked)

    }
}
