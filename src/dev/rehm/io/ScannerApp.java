package dev.rehm.io;

import java.util.Scanner;

public class ScannerApp {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Welcome, please enter your name:");
        String name = sc.nextLine();
        System.out.println("Hello "+name);
    }
}
