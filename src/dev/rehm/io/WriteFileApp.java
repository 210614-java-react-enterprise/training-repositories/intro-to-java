package dev.rehm.io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class WriteFileApp {

    public static void main(String[] args) {

        try (FileWriter fw = new FileWriter("src/dev/rehm/io/data.txt", true);
              BufferedWriter bw = new BufferedWriter(fw);) {

            bw.write("\nWe also handled any potential IOExceptions with a try/catch");

            System.out.println("wrote to file");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
