package dev.rehm.io;

import java.io.*;

public class ReadFileApp {

    public static void main(String[] args) {

        try (BufferedReader br = new BufferedReader(new FileReader("src/dev/rehm/io/data.txt"))){
            String line = br.readLine();
            while(line!=null) {
                System.out.println(line);
                line = br.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
